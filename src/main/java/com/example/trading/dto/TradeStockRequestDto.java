package com.example.trading.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class TradeStockRequestDto {
	
	private long userId;
	private long stockId;
	private String transactionType;
	private int quatity;
	private double stockPrice;

}
