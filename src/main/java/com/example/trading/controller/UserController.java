package com.example.trading.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.trading.dto.LoginRequestDto;
import com.example.trading.service.UserServiceImpl;


@RestController
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	UserServiceImpl userServiceImpl;
	
	@PostMapping("/login")
	public ResponseEntity<String> login(@Valid @RequestBody LoginRequestDto loginRequestDto) {
		String message = userServiceImpl.login(loginRequestDto);
		return new ResponseEntity<>(message, HttpStatus.OK);
		
	}
	
	
	
	

}
