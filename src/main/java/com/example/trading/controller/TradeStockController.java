package com.example.trading.controller;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.trading.dto.ResponceDto;
import com.example.trading.dto.TradeStockRequestDto;
import com.example.trading.exception.TimeOutException;
import com.example.trading.service.TradeStockService;
import com.example.trading.service.TradeStockServiceImpl;

@RestController
@RequestMapping("/trade")
public class TradeStockController {
	
	@Autowired
	TradeStockService tradeStockService;
	
	@PostMapping("")
	public ResponseEntity<ResponceDto> tradeStock(@RequestBody TradeStockRequestDto tradeStockRequestDto) throws TimeOutException {

		return new ResponseEntity<>(tradeStockService.tradeStock(tradeStockRequestDto), HttpStatus.OK);
	}
}
