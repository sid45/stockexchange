package com.example.trading.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.trading.client.StockExchangeServiceClient;
import com.example.trading.dto.StockResponseDto;

@RestController
@RequestMapping("/stock-client")
public class StockController {
	
	@Autowired
	StockExchangeServiceClient stockExchangeServiceClient;
		
	@GetMapping("")
	public ResponseEntity<StockResponseDto> getAllStocks()
	{
		return stockExchangeServiceClient.getAllStocks();
	}

}
