package com.example.trading.constants;

public class AppConstants {
	
	public static final String 	TRANSACTION_SUCCES = "transaction success ";
	
	public static final String LOGIN_SUCCESS = "LOGIN SUCCESS";
	
	public static final String LOGIN_FAILURE = "LOGIN FAILED";

	public static final String USER_NOT_FOUND = "user not found";

	public static final String QUANTITY_INSUFFICIENT = "Insufficient quantity";

}
