package com.example.trading.service;

import com.example.trading.dto.LoginRequestDto;

public interface UserService {
	
	public String login(LoginRequestDto loginRequestDto);
	
	
}


