package com.example.trading.service;

import java.time.Duration;

import java.time.LocalDateTime;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.ResponseEntity;

import org.springframework.stereotype.Service;

import com.example.trading.client.StockExchangeServiceClient;
import com.example.trading.constants.AppConstants;
import com.example.trading.dto.ResponceDto;
import com.example.trading.dto.StockRequestDto;
import com.example.trading.dto.TradeStockRequestDto;
import com.example.trading.entity.UserStock;
import com.example.trading.exception.QuantityInSufficientException;
import com.example.trading.exception.TimeOutException;
import com.example.trading.repository.UserStockRepository;

@Service
public class TradeStockServiceImpl implements TradeStockService {
	
	@Autowired
	UserStockRepository userStockRepository;
	
	
	  @Autowired
	  StockExchangeServiceClient stockExchangeServiceClient;
	  
	 

	@Override
	public ResponceDto tradeStock(TradeStockRequestDto tradeStockRequestDto) throws TimeOutException {
		Optional<UserStock> userStock=userStockRepository.findByUserIdAndStockId(tradeStockRequestDto.getUserId(),tradeStockRequestDto.getStockId());
		
		if(userStock.isPresent())
		{
			if(!tradeStockRequestDto.getTransactionType().equalsIgnoreCase(userStock.get().getTransactionType()))
			{
			Duration duration = Duration.between(LocalDateTime.now(), userStock.get().getTransactionDate());
			long seconds = duration.getSeconds();
			long hours = seconds / 60;
			if(hours<24)
				throw new TimeOutException("cannot buy / sell same stock within 24 hours");
			}
			
		}
		
		StockRequestDto  stockRequestDto= new StockRequestDto();
		stockRequestDto.setStockId(tradeStockRequestDto.getStockId());
		stockRequestDto.setQuantity(tradeStockRequestDto.getQuatity());
		stockRequestDto.setTransactionType(tradeStockRequestDto.getTransactionType());
		
		try {
			 ResponseEntity<String> result =
					  stockExchangeServiceClient.updateStock(stockRequestDto);
					  System.out.println(result.getBody());
		} catch (Exception e) {
			
			throw new QuantityInSufficientException();
		}
		 
		 
		   if(userStock.isPresent())
			{
			   userStock.get().setTransactionType(tradeStockRequestDto.getTransactionType());
			   userStock.get().setTransactionDate(LocalDateTime.now());
			   userStock.get().setQuatity(tradeStockRequestDto.getQuatity());
			   userStock.get().setAmount(tradeStockRequestDto.getQuatity()*tradeStockRequestDto.getStockPrice());
			   userStockRepository.save(userStock.get());
			}
		
		   else
		   {
			  UserStock userStock2=new UserStock();
			   BeanUtils.copyProperties(tradeStockRequestDto, userStock2);
				userStock2.setTransactionDate(LocalDateTime.now());
				userStock2.setAmount(tradeStockRequestDto.getQuatity()*tradeStockRequestDto.getStockPrice());
				userStockRepository.save(userStock2);
			  
		   }
	
	ResponceDto responceDto=new ResponceDto();
	responceDto.setMsg(AppConstants.TRANSACTION_SUCCES); 
	responceDto.setStatusCode(610);
			 return responceDto;
	}

}
