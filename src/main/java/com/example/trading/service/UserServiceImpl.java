package com.example.trading.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.trading.constants.AppConstants;
import com.example.trading.dto.LoginRequestDto;
import com.example.trading.entity.User;
import com.example.trading.repository.UserRepository;



@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserRepository userRepository;

	@Override
	public String login(LoginRequestDto loginRequestDto) {
		Optional<User> optionalUser = userRepository.findUserByEmailAndPasswordParams(loginRequestDto.getEmail(), loginRequestDto.getPassword());
		if (optionalUser.isPresent()) {
			return AppConstants.LOGIN_SUCCESS;
		} else {
			return AppConstants.LOGIN_FAILURE;
		}
		
		
	}

}
