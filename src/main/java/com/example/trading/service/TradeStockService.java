package com.example.trading.service;

import com.example.trading.dto.ResponceDto;
import com.example.trading.dto.TradeStockRequestDto;
import com.example.trading.exception.TimeOutException;

public interface TradeStockService {

	ResponceDto tradeStock(TradeStockRequestDto tradeStockRequestDto) throws TimeOutException;

}
