package com.example.trading.repository;

import java.util.Optional;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.trading.entity.UserStock;

@Repository
public interface UserStockRepository extends JpaRepository<UserStock, Long> {

	Optional<UserStock> findTopByUserIdAndTransactionType(long userId, String transactionType);

	Optional<UserStock> findByUserIdAndStockIdAndTransactionType(long userId, long stockId, String transactionType);

	Optional<UserStock> findByUserIdAndStockId(long userId, long stockId);
	
	
}
