package com.example.trading.client;

import org.springframework.cloud.openfeign.FeignClient;


import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.example.trading.dto.StockRequestDto;
import com.example.trading.dto.StockResponseDto;



@FeignClient(value="stock-service", url = "http://localhost:1010/stockexchange")
public interface StockExchangeServiceClient {
	
	 @PutMapping("/stock")
	    public ResponseEntity<String> updateStock(@RequestBody StockRequestDto stockRequestDto);
	    
	 @GetMapping("/stock")
		public ResponseEntity<StockResponseDto> getAllStocks();

}
