package com.example.trading.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="User")
public class User {
	
	@Id
	private long userId;
	
	private String userName;
	
	private String email;
	
	private String password;
	
	
	
}
