package com.example.trading.entity;

import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table
public class UserStock {
	
	@Id
	private long userStockId;
	private long userId;
	private long stockId;
	private String transactionType;
	
	private int quatity;
	
	private LocalDateTime transactionDate;
	
	private double amount; 	 	 
	
	
	
}
