package com.example.trading.exception;

public class TimeOutException extends Exception {
	
	public TimeOutException(String message) {
		super(message);
	}
}
