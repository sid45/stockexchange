package com.example.trading.service;

import static org.junit.jupiter.api.Assertions.fail;

import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.trading.constants.AppConstants;
import com.example.trading.dto.LoginRequestDto;
import com.example.trading.entity.User;
import com.example.trading.repository.UserRepository;


@SpringBootTest
class UserServiceImplTest {

	@Mock
	UserRepository userRepository;
	
	
	@InjectMocks
	UserServiceImpl userServiceImpl;
	
	@Test
	void testLogin() {
		LoginRequestDto loginRequestDto = new LoginRequestDto();
		User user = new User();
		Optional<User> optionalUser = Optional.of(user );
		Mockito.when(userRepository.findUserByEmailAndPasswordParams(Mockito.anyString(), Mockito.anyString())).thenReturn(optionalUser);
		String message = userServiceImpl.login(loginRequestDto);
		Assertions.assertEquals(AppConstants.LOGIN_FAILURE, message);;

	}

}
