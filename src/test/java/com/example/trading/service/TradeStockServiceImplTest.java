package com.example.trading.service;

import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import com.example.trading.client.StockExchangeServiceClient;
import com.example.trading.dto.ResponceDto;
import com.example.trading.dto.StockRequestDto;
import com.example.trading.dto.TradeStockRequestDto;
import com.example.trading.entity.UserStock;
import com.example.trading.exception.TimeOutException;
import com.example.trading.repository.UserStockRepository;

@SpringBootTest
class TradeStockServiceImplTest {
	@Mock
	UserStockRepository userStockRepository;
	
	@Mock
	 StockExchangeServiceClient stockExchangeServiceClient;
	
	@InjectMocks
	TradeStockServiceImpl tradeStockServiceImpl;
	TradeStockRequestDto tradeStockRequestDto;
	UserStock userStock;
	@BeforeEach
	public void setParameters() 
	{
		userStock=new UserStock();
		userStock.setTransactionType("sel");
		tradeStockRequestDto=new TradeStockRequestDto();
		tradeStockRequestDto.setTransactionType("sel");
	}

	@Test
	void tradeStock() throws TimeOutException  {
		Mockito.when(userStockRepository.findByUserIdAndStockId(Mockito.anyLong(), Mockito.anyLong())).thenReturn(Optional.of(userStock));
	  Mockito.when(userStockRepository.save(Mockito.any(UserStock.class))).thenReturn(userStock);
	  Mockito.when(stockExchangeServiceClient.updateStock(Mockito.any(StockRequestDto.class))).thenReturn(ResponseEntity.of(Optional.of("updated")));
	     ResponceDto result = tradeStockServiceImpl.tradeStock(tradeStockRequestDto);
	     Assertions.assertEquals(610, result.getStatusCode());
	}

}
