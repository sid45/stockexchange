package com.example.trading.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import com.example.trading.client.StockExchangeServiceClient;
import com.example.trading.dto.ResponceDto;
import com.example.trading.dto.TradeStockRequestDto;
import com.example.trading.exception.TimeOutException;
import com.example.trading.service.TradeStockService;

@SpringBootTest
class TradeStockControllerTest {
	


	@Mock
	TradeStockService tradeStockService;
	
	@InjectMocks
	TradeStockController tradeStockController;
	TradeStockRequestDto tradeStockRequestDto;
	
	ResponceDto responceDto;
	@BeforeEach
	public void setParameters() 
	{
		responceDto=new ResponceDto();
		responceDto.setMsg("updated");
		responceDto.setStatusCode(601);
		
		tradeStockRequestDto=new TradeStockRequestDto();
	}
	
	@Test
	void tradeStock() throws TimeOutException {
		Mockito.when(tradeStockService.tradeStock(Mockito.any(TradeStockRequestDto.class))).thenReturn(responceDto );
		ResponseEntity<ResponceDto> result = tradeStockController.tradeStock(tradeStockRequestDto);
		Assertions.assertEquals(responceDto.getStatusCode(), result.getBody().getStatusCode());
		
		
	}

}
