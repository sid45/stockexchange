package com.example.trading.controller;

import static org.junit.jupiter.api.Assertions.fail;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import com.example.trading.constants.AppConstants;
import com.example.trading.dto.LoginRequestDto;
import com.example.trading.service.UserServiceImpl;

@SpringBootTest
class UserControllerTest {
	
	@InjectMocks
	UserController userController;
	
	@Mock
	UserServiceImpl userServiceImpl;

	@Test
	void testLogin() {
		String message = AppConstants.LOGIN_SUCCESS;
		Mockito.when(userServiceImpl.login(Mockito.any(LoginRequestDto.class))).thenReturn(message );
		LoginRequestDto loginRequestDto = new LoginRequestDto();
		ResponseEntity<String> response = userController.login(loginRequestDto );
		Assertions.assertEquals(AppConstants.LOGIN_SUCCESS, response.getBody());;

	}

}
